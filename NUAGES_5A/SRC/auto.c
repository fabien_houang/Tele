#include <dirent.h>
#include <gtk/gtk.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "compute.h"
#include "k-means.h"

int process_img(char *s)
{
    guchar *pucImaOrig, *pucImaRes;
    GdkPixbuf *pixBufOrig, *pixBufRes;
    int col;
    int line;

    pixBufOrig = gdk_pixbuf_new_from_file(s, NULL);
    if (pixBufOrig == NULL)
    {
        printf("Lien vers image invalide.\n");
        return 1;
    }
    col = gdk_pixbuf_get_width(pixBufOrig);
    line = gdk_pixbuf_get_height(pixBufOrig);

    pucImaOrig = gdk_pixbuf_get_pixels(pixBufOrig);

    pixBufRes = gdk_pixbuf_copy(pixBufOrig);
    pucImaRes = gdk_pixbuf_get_pixels(pixBufRes);
    ComputeImage(pucImaOrig, line, col, pucImaRes);

    int ans = compute_cloud(pucImaRes, line, col);
    printf("%s: %i %% de nuage.\n", s, ans);

    return 0;
}

int main(int argc, char **argv)
{
    int errcode = 0;
    if (argc < 2)
    {
        DIR *d;
        struct dirent *dir;
        d = opendir(".");
        if (d)
        {
            dir = readdir(d);
            while (dir != NULL)
            {
                if (!strcmp(dir->d_name + (strlen(dir->d_name) - 4), ".bmp"))
                    errcode |= process_img(dir->d_name);

                dir = readdir(d);
            }
            closedir(d);
        }
    }
    else
        for (int ind = 1; ind < argc; ind++)
            errcode |= process_img(argv[ind]);
    return errcode;
}
