#include "k-means.h"

// sort element in tmp in decreasing order
//
// arg:
//  elt is the vector that will contain sorted elements
//  tmp is the vector that contains element to sort
void sort(guchar* elt, guchar* tmp, int nb_elt)
{
    for (int i = 0; i < nb_elt; i++)
    {
        guchar max = tmp[0];
        int index = 0;
        for (int j = 0; j < nb_elt; j++)
        {
            if (tmp[j] > max)
            {
                max = tmp[j];
                index = j;
            }
        }
        elt[i] = max;
        tmp[index] = 0;
    }
}

// returns an array of vectors containing five pixels values
//
// arg:
//  grey_img is the image in grey level
//  l is the number of line
//  c is the number of column
guchar** img_to_vector(guchar *grey_img, int l, int c)
{
    guchar** ans = malloc(l * c * sizeof(guchar*));

    // for each pixel
    for (int i = 0; i < 3 * l * c; i += 3)
    {
        guchar* elt = malloc(5 * sizeof(guchar));
        guchar tmp[5] = { 0 };
        // assign_value(elt, l, c, grey_img[i]);
        // not on first row, can check top pixel
        if (i >= 3 * c)
            tmp[0] = grey_img[i - 3 * c];
        // check left pixel
        if (i % c != 0)
            tmp[1] = grey_img[i - 3];
        // check right pixel
        if (i + 3 % c != 0)
            tmp[2] = grey_img[i + 3];
        // check bottom pixel
        if (i / (3 * c) < l - 1)
            tmp[3] = grey_img[i + 3 * c];
        tmp[4] = grey_img[i];

        sort(elt, tmp, 5);
        ans[i / 3] = elt;
    }
    return ans;
}

// returns 1 if it is the same, 0 otherwise
int compare_vector(guchar *v, guchar *v2)
{
    for (int i = 0; i < 5; i++)
        if (v[i] != v2[i])
            return 0;
    return 1;
}

void assign(guchar *v, guchar *v2)
{
    for (int i = 0; i < 5; i++)
        v[i] = v2[i];
}

void assign_value(guchar *v, guchar value)
{
    for (int i = 0; i < 5; i++)
        v[i] = value;
}

int get_norm(guchar *v)
{
    return sqrt(v[0] * v[0] + v[1] * v[1] + v[2] * v[2]
            + v[3] * v[3] + v[4] * v[4]);
}

int dist(guchar *v, guchar *v2)
{
    guchar ans[5];
    for (int i = 0; i < 5; i++)
        ans[i] = abs(v[i] - v2[i]);
    return get_norm(ans);
}

int mean(guchar **v, int nb)
{
    int ans = 0;
    for (int i = 0; i < nb; i++)
        for (int j = 0; j < 5; j++)
            ans += v[i][j];
    return ans / (nb * 5);
}

// returns the percentage of cloud in the image
//
// arg:
//  grey_img is the image in grey level
//  l is the number of line
//  c is the number of column
int compute_cloud(guchar *grey_img, int l, int c)
{
    guchar cloud_lvl[5] = { 0 };
    guchar grnd_lvl[5] = { 0 };
    guchar sea_lvl[5] = { 0 };

    guchar **vector = img_to_vector(grey_img, l, c);
    guchar new_cloud_lvl[5] = { 255, 255, 255, 255, 255 };
    guchar new_grnd_lvl[5] = { 250, 250, 250, 250, 250 };
    guchar new_sea_lvl[5] = { 240, 240, 240, 240, 240 };

    guchar* cloud_cluster[l * c];
    int nb_cloud = 0;
    guchar* grnd_cluster[l * c];
    int nb_grnd = 0;
    guchar* sea_cluster[l * c];
    int nb_sea = 0;

    // k mean calculus
    while (!(compare_vector(new_cloud_lvl, cloud_lvl)
                && compare_vector(new_grnd_lvl, grnd_lvl)
                && compare_vector(new_sea_lvl, sea_lvl)))
    {
        // initialization
        nb_cloud = 0;
        nb_grnd = 0;
        nb_sea = 0;

        assign(cloud_lvl, new_cloud_lvl);
        assign(grnd_lvl, new_grnd_lvl);
        assign(sea_lvl, new_sea_lvl);

        // compute clusters
        for (int i = 0; i < l * c; i++)
        {
            if (dist(vector[i], cloud_lvl)
                    < dist(vector[i], grnd_lvl))
            {
                cloud_cluster[nb_cloud] = vector[i];
                nb_cloud++;
            }
            else if (dist(vector[i], sea_lvl)
                    > dist(vector[i], grnd_lvl))
            {
                grnd_cluster[nb_grnd] = vector[i];
                nb_grnd++;
            } else {
                sea_cluster[nb_sea] = vector[i];
                nb_sea++;
            }
        }

        // compute new means
        if (nb_cloud != 0)
        {
            int m = mean(cloud_cluster, nb_cloud);
            assign_value(new_cloud_lvl, m);
        }

        if (nb_grnd != 0)
        {
            int m = mean(grnd_cluster, nb_grnd);
            assign_value(new_grnd_lvl, m);
        }

       if (nb_sea != 0)
        {
            int m = mean(sea_cluster, nb_sea);
            assign_value(new_sea_lvl, m);
        }

    printf("ground : %d\ncloud : %d, nb_cloud : %d \n", grnd_lvl[0], cloud_lvl[0], nb_cloud);
    }



    // change color of clouds
    for (int i = 0; i < 3 * l * c; i += 3)
        if (dist(vector[i / 3], cloud_lvl)
                < dist(vector[i / 3], grnd_lvl))
        {
            grey_img[i] = 0;
            grey_img[i + 1] = 255;
            grey_img[i + 2] = 0;
        }
    //    printf("Moyenne sol %i, moyenne nuage %i", new_grnd_lvl[0],
    //          new_cloud_lvl[0]);

    return nb_cloud * 100 / (l * c);
}
